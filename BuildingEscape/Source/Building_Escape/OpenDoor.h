// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "OpenDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDING_ESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	void OpenDoor(float DeltaTime);
	void CloseDoor(float DeltaTime);
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(EditAnywhere, Category="Door")
	float OpeningAngle = 90.f;
	UPROPERTY(EditAnywhere, Category="Door")
	ATriggerVolume* PressurePlate;
	UPROPERTY(EditAnywhere, Category="Door")
	AActor* PlayerActor;

	FRotator TargetRotation;
	FRotator InitialRotation;

	float DoorLastOpened = 0.f;
	float DoorCloseDelay = 0.1f;
};
