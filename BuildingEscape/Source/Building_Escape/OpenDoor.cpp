// Fill out your copyright notice in the Description page of Project Settings.


#include "OpenDoor.h"
#include "GameFramework/Actor.h"

// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	UWorld* World = GetWorld();
	APlayerController* PlayerController = nullptr;

	InitialRotation = GetOwner()->GetActorRotation();
	TargetRotation = InitialRotation;
	TargetRotation.Yaw += OpeningAngle;

	PlayerActor = GetWorld()->GetFirstPlayerController()->GetPawn();
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(PressurePlate && PlayerActor && PressurePlate->IsOverlappingActor(PlayerActor))
	{
		OpenDoor(DeltaTime);
		DoorLastOpened = GetWorld()->GetTimeSeconds();
	} else if((DoorLastOpened + DoorCloseDelay) < GetWorld()->GetTimeSeconds())
	{
		CloseDoor(DeltaTime);
	}
}

void UOpenDoor::OpenDoor(float DeltaTime)
{
	FRotator Rotation = GetOwner()->GetActorRotation();
	Rotation = FMath::RInterpTo(Rotation, TargetRotation, DeltaTime, 2);
	GetOwner()->SetActorRotation(Rotation);
}

void UOpenDoor::CloseDoor(float DeltaTime)
{
	FRotator Rotation = GetOwner()->GetActorRotation();
	Rotation = FMath::RInterpTo(Rotation, InitialRotation, DeltaTime, 4);
	GetOwner()->SetActorRotation(Rotation);
}

